const gulp = require('gulp'),
  pug = require('gulp-pug'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  watch = require('gulp-watch'),
  browserSync = require('browser-sync'),
  reload = browserSync.reload,
  sourcemaps = require('gulp-sourcemaps'),
  imagemin = require('gulp-imagemin');

// Compiling Pug templates
gulp.task('pug', function buildHTML() {
  return gulp.src('app/views/**/*.pug')
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest('dist'));
});


// Compiling Sass files
gulp.task('sass', function () {
  return gulp.src('app/styles/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error',sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/styles'))
    .pipe(sass({outputStyle: 'compressed'}));
});

gulp.task('sass:watch', function () {
  gulp.watch('app/styles/scss/**/*.scss', ['sass']);
});

// Compiling Script files
gulp.task('script', function () {
    return gulp.src('app/scripts/**/*.js')
        .pipe(gulp.dest('dist/scripts'));
});

gulp.task('script:watch', function () {
    gulp.watch('app/scripts/**/*.js', ['script']);
});

// Copy images
gulp.task('copy-images', function () {
  gulp
    .src('app/images/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/images'));
});

// Copy fonts
gulp.task('copy-fonts', function () {
  gulp
    .src('app/styles/fonts/**/*')
    .pipe(gulp.dest('dist/styles/fonts'));
});

// Watch files for changes & reload
gulp.task('serve', () => {
  browserSync({
    notify: false,
    open: false,
    logPrefix: 'WSK',
    scrollElementMapping: ['main', '.mdl-layout'],
    server: ['dist', 'app'],
    port: 3012,
  });

    gulp.watch(['app/views/**/*.pug'], ['pug', reload]);
    gulp.watch(['app/styles/scss/**/*.scss'], ['sass', reload]);
    gulp.watch(['app/scripts/**/*.js'], ['script', reload]);
    gulp.watch(['app/images/**/*'], reload);
});

gulp.task('default', ['serve']);
gulp.task('dist', ['pug', 'sass', 'script', 'copy-images', 'copy-fonts']);

