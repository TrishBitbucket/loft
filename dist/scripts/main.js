$(function() {
    'use strict';

    $(".checkbox").on("click", function(){
        var courseItem = $(this).closest(".course-item");

        if( $(this).is(':checked') ) {
            courseItem.addClass('course-viewed').fadeOut( "slow" );
        }

        if(courseItem.siblings(".course-item:not('.course-viewed')").length == 0 ) {
            setTimeout(function(){
                $(".passed-txt").fadeIn( "slow" );
            }, 1000);
        }
    });
});




